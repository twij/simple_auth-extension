<?php namespace Hotfootdesign\SimpleAuthExtension\Http\Controller;

use Anomaly\Streams\Platform\Http\Controller\PublicController;
use Anomaly\Streams\Platform\View\ViewTemplate;
use Illuminate\Http\Request;
use Anomaly\VariablesModule\Variable\Contract\VariableRepositoryInterface;
use Anomaly\Streams\Platform\Message\MessageBag;


class SimpleAuthController extends PublicController
{
    private $variables;

    public function __construct(VariableRepositoryInterface $variables){
        $this->variables = $variables;
    }

    public function auth(Request $request, MessageBag $messages)
    {
        //security is fun
        $pass = $request->input('pass');

        $correctPass = $this->variables->get('site_variables', 'password');

        if($pass == $correctPass){
            \session(['simple-auth' => true]);
            return 'true';
        }else{
            $messages->error('Incorrect password entered.');
            return 'false';
        }
    }

}
