$('#pass').focus();

$("#enter").click(function(){
  auth();
});

$(document).keypress(function(e) {
  if(e.which == 13) {
    auth();
  }
});

function auth(){
  var pass = $('#pass').val();

  $.ajax({
      url: "/guides/auth",
      method: "post",
      data: { 'pass': pass, "_token": CSRF_TOKEN, }
  }).done(function(msg) {
      $('#pass').val('');
      location.reload();
  }).error(function(msg){
      console.log(msg);
      $('#pass').val('');
      $('#error-eg').html('An error occurred. Please try again in 1 minute.');
  });
}
