#Simple Auth plugin
This extension adds a simple and basic way to password protect a page in PyroCMS using a session variable. This is not particularly secure.

##Installation
Add the repository to your project's *composer.json* file:
~~~~
    "repositories": [
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:hotfootdesign/simple_auth-extension.git"
        }
    ]
~~~~

Add the extension to *require*
~~~~
    "require": {
      "hotfootdesign/simple_auth-extension": "dev-master"
    }
~~~~

Install the extension with
~~~~
    php artisan extension:install simple_auth
~~~~

##Usage
You must create a variable in PyroCMS. The default name is 'password' but you can change that in the controller file.

To use on a page do something like:

~~~
    {% if session_has('simple-auth') %}
      <strong>Secret content goes here.</strong>
    {% else %}
      <div class="login-form">
        <h1>Enter password to continue</h1>
        <div class="controls">
          <input type="password" id="pass">
          <button type="button" id="enter">Unlock</button>
        </div>
        <div class="error-eg"></div>
      </div>
    {% endif %}

    <script src="{{ hotfootdesign.extention.simple_auth::js/auth.js }}"></script>
~~~
